package main

import (
	"fmt"
	"github.com/chromedp/chromedp"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
	"log"
	"os"
)

func main() {
	ctx := context.Background()
	b, err := os.ReadFile("../config/credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}
	config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	c := getClient(config)

	sheetsService, err := sheets.New(c)
	if err != nil {
		log.Fatal(err)
	}

	spreadsheetId := "13xJHpsaLk_dFg0hUGysZM9w4zsbZGdTWD0fNMlpjiMk"

	range2 := "A2:A156"

	valueRenderOption := "FORMATTED_VALUE"

	dateTimeRenderOption := "SERIAL_NUMBER"

	response, err := sheetsService.Spreadsheets.Values.Get(spreadsheetId, range2).ValueRenderOption(valueRenderOption).DateTimeRenderOption(dateTimeRenderOption).Context(ctx).Do()
	if err != nil {
		log.Fatal(err)
	}

	var urls []string
	for _, arr := range response.Values {
		for _, url := range arr {
			urls = append(urls, url.(string))
		}
	}
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()
	valueInputOption := "USER_ENTERED"
	range2 = "E2:E156"
	var values [][]interface{}
	for _, url := range urls {
		fmt.Println(url)
		var text string
		if err := chromedp.Run(ctx,
			chromedp.Navigate(url),
			chromedp.Text("body", &text),
		); err != nil {
			fmt.Println("Failed to get text:", err)
		}
		words := Check(text)
		values = append(values, []interface{}{words})
	}
	rb := &sheets.ValueRange{
		Values: values,
	}
	resp, err := sheetsService.Spreadsheets.Values.Update(spreadsheetId, range2, rb).ValueInputOption(valueInputOption).Context(ctx).Do()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%#v\n", resp)
}
